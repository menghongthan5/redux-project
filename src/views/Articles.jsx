import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { fetchAllArticles } from '../redux/actions/articlesAction'


export default function Articles() {
    const {articles, isLoading} = useSelector(state=>state.articlesReducer)
    //console.log("DAATA:", data);
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(fetchAllArticles())
    },[])

    return (
        <div>
            {
                isLoading?(<h1>Loading...</h1>):(<h1>Articles</h1>)
            }
        </div>
    )
}
