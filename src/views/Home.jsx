import React from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { decrease, increase } from "../redux/actions/counterAction";
import * as counterAction from "../redux/actions/counterAction";

export default function Home() {
  const data = useSelector((state) => state);
  const dispatch = useDispatch();
  console.log("DATA:", data);

  const { increase, decrease } = bindActionCreators(counterAction, dispatch);
  //console.log("BIND:", bind);

  function onIncrease() {
    // dispatch(increase("dara"));
    increase("dara")
  }

  function onDecrease() {
    // dispatch(decrease());
    decrease()
  }

  return (
    <div>
      <h1>Counter: {data.counter}</h1>
      <button onClick={onIncrease}>Increase</button>
      <button onClick={onDecrease}>Decrease</button>
      {/* <button onClick={()=>dispatch({type: "INCREASE", user: "dara"})}>Increase</button> */}
    </div>
  );
}
